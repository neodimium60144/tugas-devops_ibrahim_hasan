# Todo
Simple Todo App

Dalam sebuah perusahaan Andi bekerja sebagai DevOps, jelaskan menurut pendapatmu definisi tentang DevOps dan seberapa penting DevOps di suatu perusahaan serta gambarkan flow DevOps dari development hingga ke production? BOBOT MAX: 10
Seorang DevOps akan selalu berhubungan dengan server, dalam praktiknya DevOps akan mengkonfigurasi lebih dari 5 server. Jelaskan bagaimana cara kamu melakukan konfigurasi server-server dengan tepat waktu sesuai deadline pekerjaanmu? BOBOT MAX: 5
Apa yang kamu ketahui mengenai TCP/IP dalam sebuah jaringan komputer? BOBOT MAX: 5
Definisikan database SQL dan NoSQL menurut Kamu, serta berikan contoh dari masing-masing database yang telah Kamu sebutkan? BOBOT MAX: 5
Berikan contoh penggunaan Git dan jelaskan perintah yang dijalankan tersebut, minimal 5 perintah Git! Sertakan screenshot step by stepnya atau record menjadi video. MAX SCORE: 20
Kita sering mengenal platform yang bernama GitLab, Github dan Bitbucket, ketiga platform tersebut memiliki keunggulan masing-masing yang mendukung pekerjaan seorang DevOps, menurut pendapat kamu dari ketiga platform tersebut berikan alasan kenapa harus memilih 1 di antara yang lain? MAX SCORE: 5
Deploy sebuah project yang telah Kami sediakan ke Heroku menggunakan heroku-cli sertakan screenshot dan deskripsikan step by step-nya atau record menjadi video. MAX SCORE: 50
Clone repository di bawah ini ke komputer lokal
https://github.com/sgnd/todo
Ganti config database “production”
config/config.json
Menggunakan database SQL

JAWABAN = 
1).Flow Devops dari developement sampai production
Singkatnya yang dilakukan devops adalah continous integration ,Continous delivery, configuration management, infrastructure as a code, monitoring, logging. communication and collaboration.

a)Continuous Integration adalah layanan yang melakukan build dan pengujian otomatis, yang terintegrasi dengan hosting repository source code (seperti Github, Gitlab, atau Bitbucket). Di mana setiap kita melakukan push commit-commit baru, atau ada pull request ke dalam repository project, layanan ini langsung menjalankan automated build dan test, sehingga jika ada error atau kegagalan test akan segera ketahuan. Jika terjadi kesalahan/bug dalan build/test, Development Team dapat dengan cepat melakukan perbaikan code sehingga kesalahan yang terjadi dapat di tanggulangi dan mengurangi waktu untuk melakukan validasi sebuah update.
b)Continuous Delivery adalah praktek di dalam software development dimana para Software Developer yang melakukan perubahan pada kode, sudah melakukan build & test yang dijalankan otomatis oleh Continuous Integration dan siap untuk deploy ke environment production namun memilih untuk tidak men-deploy pada umumnya. Ketika Continuous Delivery dijalankan dengan baik, tim Software Developer akan selalu memiliki sebuah bagian aplikasi atau aplikasi itu sendiri yang siap untuk di deploy ke environment production. Ini dilakukan untuk membantu mengurangi biaya, waktu, dan risiko dalam perubahan aplikasi dengan memungkinkan untuk menambah update lebih banyak untuk aplikasi yang sedang berjalan. Sebuah proses penyebaran langsung dan berulang sangat penting dalam Continuous Delivery (CD).
Berikut beberapa manfaat dari pengiriman yang berkesinambungan dapat dirangkum:
Mempercepat Waktu rilis : CD memungkinkan tim ataupun orang lain memberikan nilai bisnis yang ada dalam rilis software baru lebih cepat ke users. Kemampuan ini membantu perusahaan tetap selangkah lebih maju dari para pesaing.
Membangun Produk Berkualitas: Rilis yang lebih sering memberikan Software Development mendapatkan response yang lebih cepat dari para pengguna. Hal ini memungkinkan mereka untuk bekerja hanya pada fitur yang berguna. Jika mereka menemukan bahwa fitur tersebut tidak berguna, mereka akan hentikan usaha lebih lanjut terhadap fitur tersebut. Ini membantu mereka dalam pembangunan produk software yang tepat.
Peningkatan Produktivitas dan Efisiensi: Penghematan waktu yang signifikan untuk Development, Testing, DevOps, dll melalui Automation.
Kehandalan Rilis: Risiko yang terkait dengan rilis dapat menurun secara signifikan, sehingga proses rilis menjadi lebih handal. Dengan CD, proses penyebaran dan skrip diuji berulang kali sebelum di jalankan. Jadi, sebagian besar kesalahan script dalam proses penyebaran dapat ditemukan. Dengan kemampuan membuat rilis sesering mungkin, jumlah perubahan kode di setiap rilis tentu menurun. Hal ini menjadikan mudah dalam menemukan dan memperbaiki masalah yang terjadi, mengurangi waktu di mana mereka memiliki dampak.
Peningkatan Kualitas Produk: Jumlah bug terbuka dan insiden produksi telah menurun secara signifikan.
Peningkatan Kepuasan Pelanggan: Tingkat kepuasan pelanggan yang lebih tinggi dapat tercapai.
c)Continuous Deployment merupakan salah satu rangkaian setelah Continuous Integration dan Continuous Delivery selesai dijalankan. Umumnya organisasi/perusahaan memiliki environment test / development, dan disinilah fungsi utama continuous deployment, yaitu ketika hasil dari Continuous Integration sudah dinyatakan baik, tim pengembang dapat segera melihat perubahan pada environment test / environment development / environment production.
d)Continuous Deployment merupakan cara yang sangat baik untuk meningkatkan jumlah feedback yang diterima oleh pengguna aplikasi dan mengurangi tekanan karena tidak ada lagi “Hari Rilis”. Tim pengembang dapat fokus dalam membangun produk dan dapat melihat perubahan code mereka beberapa menit setelah mereka selesai mengerjakannya.
e)Configuration Management
Configuration Management adalah praktek dalam proses System Engineering yang memiliki tujuan untuk me-maintain konfigurasi sebuah produk, dan memastikan konsitensinya dalam seluruh environment. Dengan menggunakan Configuration Management, proses konfigurasi produk dapat diotomatisasi, distandardisasi dan mengurangi proses konfigurasi yang manual. Tahap selanjutnya, Configuration Management akan mempermudah dalam konfigurasi banyak server dan dapat meminimalisir kesalahan, karena konfigurasi ditulis dalam code, tidak lagi menjalankan perintah manual.
f)Infrastructure as a Code (IAAC)
Infrastructure as a Code adalah sebuah praktek dalam System Architecture yang mana infrastruktur sebuah produk didefinisikan dalam code yang dapat diprogram, distandardiasikan dan mudah untuk di duplikasi atau bisa disebut juga bahasa kerennya Programmable Infrastructure. Intinya yaitu kita tidak membangun server secara manual, cukup dengan code seperti kita mengkoding kita dapat menambahkan server. Produk skala menengah, mungkin membutuhkan lebih dari satu mesin. Dengan IAAC, tim pengembang dapat dengan mudah menambah mesin melalui satu baris kode.
Produk dalam skala besar, umumnya bergantung pada beberapa produk yang ditawarkan oleh provider cloud, seperti provider aws yang menawarkan produk seperti AWS EC2 dan AWS Kinesis Data Stream. Dengan IAAC, tim pengembang tidak lagi melakukan operasi manual pada console provider cloud, cukup dengan merubah beberapa baris kode, hal yang lebih penting yaitu dapat meminimalisir kesalahan.


2).Cara konfigurasi server
cara biasa adalah menggunakan nginx,

cara yang lebih efisien adalah dengan menggunakan configuration management contohnya adalah seperti ansible.
Configuration Management
Configuration Management adalah praktek dalam proses System Engineering yang memiliki tujuan untuk me-maintain konfigurasi sebuah produk, dan memastikan konsitensinya dalam seluruh environment. Dengan menggunakan Configuration Management, proses konfigurasi produk dapat diotomatisasi, distandardisasi dan mengurangi proses konfigurasi yang manual. Tahap selanjutnya, Configuration Management akan mempermudah dalam konfigurasi banyak server dan dapat meminimalisir kesalahan, karena konfigurasi ditulis dalam code, tidak lagi menjalankan perintah manual.
Ansible adalah alat otomasi untuk penyediaan, penyebaran aplikasi, dan manajemen konfigurasi. Dengan meggunakan SSH untuk masuk ke server untuk menjalankan perintah atau mengakses bersama-sama dengan skrip bash/terminal untuk semi-otomatis mengotomatiskan task yang sulit sekalipun. Apakah kamu saat mengelola server secara manual atau fullsatck , Ansible tidak hanya dapat menyederhanakan proses tersebut , tetapi juga menghemat waktu Anda.

Perintah yang Anda jalankan melalui Ansible adalah idempoten, memungkinkan Anda untuk menjalankannya dengan aman beberapa kali tanpa ada yang diubah, kecuali diminta. Perlu memastikan Nginx diinstal pada semua host? Jalankan saja perintah dan Ansible akan memastikan hanya mereka yang tidak memiliki perangkat lunak yang akan menginstalnya. Semua host lain akan tetap tidak tersentuh.


3).TCP/IP dalam jaringan komputer

Suit protokol internet merupakan model jaringan komputer dan rangkaian protokol komunikasi yang digunakan di internet dan jaringan komputer yang mirip. Ia dikenal dengan TCP/IP (singkatan dari Transmission Control Protocol/Internet Protocol) yang diterjemahkan menjadi Protokol Kendali Transmisi/Protokol Internet, yang merupakan gabungan dari protokol TCP (Transmission Control Protocol) dan IP (Internet Protocol) sebagai sekelompok protokol yang mengatur komunikasi data dalam proses tukar-menukar data dari satu komputer ke komputer lain di dalam jaringan internet yang akan memastikan pengiriman data sampai ke alamat yang dituju. Protokol ini tidaklah dapat berdiri sendiri, karena memang protokol ini berupa kumpulan protokol (protocol suite). Protokol ini juga merupakan protokol yang paling banyak digunakan saat ini, karena protokol ini mampu bekerja dan diterapkan pada lintas perangkat lunak dalam berbagai sistem operasi Istilah yang diberikan kepada perangkat lunak ini adalah TCP/IP stack. 

Arsitektur TCP/IP tidaklah berbasis model referensi tujuh lapis OSI, tetapi menggunakan model referensi DARPA. Seperti diperlihatkan dalam diagram, TCP/IP merngimplemenasikan arsitektur berlapis yang terdiri atas empat lapis. Empat lapis ini, dapat dipetakan (meski tidak secara langsung) terhadap model referensi OSI. Empat lapis ini, kadang-kadang disebut sebagai DARPA Model, Internet Model, atau DoD Model, mengingat TCP/IP merupakan protokol yang awalnya dikembangkan dari proyek ARPANET yang dimulai oleh Departemen Pertahanan Amerika Serikat.
Setiap lapisan yang dimiliki oleh kumpulan protokol (protocol suite) TCP/IP diasosiasikan dengan protokolnya masing-masing. Protokol utama dalam protokol TCP/IP adalah sebagai berikut:
Protokol lapisan aplikasi: bertanggung jawab untuk menyediakan akses kepada aplikasi terhadap layanan jaringan TCP/IP. Protokol ini mencakup protokol Dynamic Host Configuration Protocol (DHCP), Domain Name System (DNS), Hypertext Transfer Protocol (HTTP), File Transfer Protocol (FTP), Telnet, Simple Mail Transfer Protocol (SMTP), Simple Network Management Protocol (SNMP), dan masih banyak protokol lainnya. Dalam beberapa implementasi stack protokol, seperti halnya Microsoft TCP/IP, protokol-protokol lapisan aplikasi berinteraksi dengan menggunakan antarmuka Windows Sockets (Winsock) atau NetBIOS over TCP/IP (NetBT).
Protokol lapisan antar-host: berguna untuk membuat komunikasi menggunakan sesi koneksi yang bersifat connection-oriented atau broadcast yang bersifat connectionless. Protokol dalam lapisan ini adalah Transmission Control Protocol (TCP) dan User Datagram Protocol (UDP).
Protokol lapisan internetwork: bertanggung jawab untuk melakukan pemetaan (routing) dan enkapsulasi paket-paket data jaringan menjadi paket-paket IP. Protokol yang bekerja dalam lapisan ini adalah Internet Protocol (IP), Address Resolution Protocol (ARP), Internet Control Message Protocol (ICMP), dan Internet Group Management Protocol (IGMP).
Protokol lapisan antarmuka jaringan: bertanggung jawab untuk meletakkan frame-frame jaringan di atas media jaringan yang digunakan. TCP/IP dapat bekerja dengan banyak teknologi transport, mulai dari teknologi transport dalam LAN (seperti halnya Eternet dan Token Ring), MAN dan WAN (seperti halnya dial-up modem yang berjalan di atas Public Switched Telephone Network (PSTN), Integrated Services Digital Network (ISDN), serta Asynchronous Transfer Mode (ATM)).
Protokol TCP/IP menggunakan dua buah skema pengalamatan yang dapat digunakan untuk mengidentifikasikan sebuah komputer dalam sebuah jaringan atau jaringan dalam sebuah internetwork, yakni sebagai berikut:
Pengalamatan IP: yang berupa alamat logis yang terdiri atas 32-bit (empat oktet berukuran 8-bit) yang umumnya ditulis dalam format www.xxx.yyy.zzz. Dengan menggunakan subnet mask yang diasosiasikan dengannya, sebuah alamat IP pun dapat dibagi menjadi dua bagian, yakni Network Identifier (NetID) yang dapat mengidentifikasikan jaringan lokal dalam sebuah internetwork dan Host identifier (HostID) yang dapat mengidentifikasikan host dalam jaringan tersebut. Sebagai contoh, alamat 205.116.008.044 dapat dibagi dengan menggunakan subnet mask 255.255.255.000 ke dalam Network ID 205.116.008.000 dan Host ID 44. Alamat IP merupakan kewajiban yang harus ditetapkan untuk sebuah host, yang dapat dilakukan secara manual (statis) atau menggunakan Dynamic Host Configuration Protocol (DHCP) (dinamis).
Fully qualified domain name (FQDN): Alamat ini merupakan alamat yang direpresentasikan dalam nama alfanumerik yang diekspresikan dalam bentuk <nama_host>.<nama_domain>, di mana <nama_domain> mengindentifikasikan jaringan di mana sebuah komputer berada, dan <nama_host> mengidentifikasikan sebuah komputer dalam jaringan. Pengalamatan FQDN digunakan oleh skema penamaan domain Domain Name System (DNS). Sebagai contoh, alamat FQDN id.wikipedia.org merepresentasikan sebuah host dengan nama "id" yang terdapat di dalam domain jaringan "wikipedia.org". Nama domain wikipedia.org merupakan second-level domain yang terdaftar di dalam top-level domain .org, yang terdaftar dalam root DNS, yang memiliki nama "." (titik). Penggunaan FQDN lebih bersahabat dan lebih mudah diingat ketimbang dengan menggunakan alamat IP. Akan tetapi, dalam TCP/IP, agar komunikasi dapat berjalan, FQDN harus diterjemahkan terlebih dahulu (proses penerjemahan ini disebut sebagai resolusi nama) ke dalam alamat IP dengan menggunakan server yang menjalankan DNS, yang disebut dengan Name Server atau dengan menggunakan berkas hosts (/etc/hosts atau %systemroot%\system32\drivers\etc\hosts) yang disimpan di dalam mesin yang bersangkutan.
Layanan
Berikut ini merupakan layanan tradisional yang dapat berjalan di atas protokol TCP/IP:
Pengiriman berkas (file transfer). File Transfer Protocol (FTP) memungkinkan pengguna komputer yang satu untuk dapat mengirim ataupun menerima berkas ke sebuah host di dalam jaringan. Metode otentikasi yang digunakannya adalah penggunaan nama pengguna (user name) dan password'', meskipun banyak juga FTP yang dapat diakses secara anonim (anonymous), alias tidak berpassword. (Keterangan lebih lanjut mengenai FTP dapat dilihat pada RFC 959.)
Remote login. Network terminal Protocol (telnet) memungkinkan pengguna komputer dapat melakukan log in ke dalam suatu komputer di dalam suatu jaringan secara jarak jauh. Jadi hal ini berarti bahwa pengguna menggunakan komputernya sebagai perpanjangan tangan dari komputer jaringan tersebut. (Keterangan lebih lanjut mengenai Telnet dapat dilihat pada RFC 854 dan RFC 855.)
Computer mail. Digunakan untuk menerapkan sistem surat elektronik. (Keterangan lebih lanjut mengenai e-mail dapat dilihat pada RFC 821 RFC 822.)
Network File System (NFS). Pelayanan akses berkas-berkas yang dapat diakses dari jarak jauh yang memungkinkan klien-klien untuk mengakses berkas pada komputer jaringan, seolah-olah berkas tersebut disimpan secara lokal. (Keterangan lebih lanjut mengenai NFS dapat dilihat RFC 1001 dan RFC 1002.)
Remote execution. Memungkinkan pengguna komputer untuk menjalankan suatu program tertentu di dalam komputer yang berbeda. Biasanya berguna jika pengguna menggunakan komputer yang terbatas, sedangkan ia memerlukan sumber yg banyak dalam suatu sistem komputer.
 Ada beberapa jenis remote execution, ada yang berupa perintah-perintah dasar saja, yaitu yang dapat dijalankan dalam system komputer yang sama dan ada pula yg menggunakan sistem Remote Procedure Call (RPC), yang memungkinkan program untuk memanggil subrutin yang akan dijalankan di sistem komputer yg berbeda. (sebagai contoh dalam Berkeley UNIX ada perintah rsh dan rexec.)
Name server yang berguna sebagai penyimpanan basis data nama host yang digunakan pada Internet (Keterangan lebih lanjut dapat dilihat pada RFC 822 dan RFC 823 yang menjelaskan mengenai penggunaan protokol name server yang bertujuan untuk menentukan nama host di Internet.)
Request for Comments
RFC (Request For Comments) merupakan standar yang digunakan dalam Internet, meskipun ada juga isinya yg merupakan bahan diskusi ataupun omong kosong belaka. Diterbitkan oleh IAB yang merupakan komite independen yang terdiri atas para peneliti dan profesional yang mengerti teknis, kondisi dan evolusi Internet. Sebuah surat yg mengikuti nomor RFC menunjukan status RFC:
S: Standard, standar resmi bagi internet
DS: Draft standard, protokol tahap akhir sebelum disetujui sebagai standar
PS: Proposed Standard, protokol pertimbangan untuk standar masa depan
I: Informational, berisikan bahan-bahan diskusi yang sifatnya informasi
E: Experimental, protokol dalam tahap percobaan tetapi bukan pada jalur standar.
H: Historic, protokol-protokol yang telah digantikan atau tidak lagi dipertimbangkan untuk standardisasi.
Bentuk arsitektur
Dikarenakan TCP/IP adalah serangkaian protokol di mana setiap protokol melakukan sebagian dari keseluruhan tugas komunikasi jaringan, maka tentulah implementasinya tak lepas dari arsitektur jaringan itu sendiri. Arsitektur rangkaian protokol TCP/IP mendifinisikan berbagai cara agar TCP/IP dapat saling menyesuaikan.
Karena TCP/IP merupakan salah satu lapisan protokol Model OSI, berarti bahwa hierarki TCP/IP merujuk kepada 7 lapisan OSI tersebut. Tiga lapisan teratas biasa dikenal sebagai "upper level protocol" sedangkan empat lapisan terbawah dikenal sebagai "lower level protocol". Tiap lapisan berdiri sendiri tetapi fungsi dari masing-masing lapisan bergantung dari keberhasilan operasi layer sebelumnya. Sebuah lapisan pengirim hanya perlu berhubungan dengan lapisan yang sama di penerima (jadi misalnya lapisan data link penerima hanya berhubungan dengan lapisan data link pengirim) selain dengan satu layer di atas atau di bawahnya (misalnya lapisan network berhubungan dengan lapisan transport di atasnya atau dengan lapisan data link di bawahnya).
Model dengan menggunakan lapisan ini merupakan sebuah konsep yang penting karena suatu fungsi yang rumit yang berkaitan dengan komunikasi dapat dipecahkan menjadi sejumlah unit yang lebih kecil. Tiap lapisan bertugas memberikan layanan tertentu pada lapisan diatasnya dan juga melindungi lapisan diatasnya dari rincian cara pemberian layanan tersebut. Tiap lapisan harus transparan sehingga modifikasi yang dilakukan atasnya tidak akan menyebabkan perubahan pada lapisan yang lain. Lapisan menjalankan perannya dalam pengalihan data dengan mengikuti peraturan yang berlaku untuknya dan hanya berkomunikasi dengan lapisan yang setingkat. Akibatnya sebuah layer pada satu sistem tertentu hanya akan berhubungan dengan lapisan yang sama dari sistem yang lain. Proses ini dikenal sebagai Peer process. Dalam keadaan sebenarnya tidak ada data yang langsung dialihkan antar lapisan yang sama dari dua sistem yang berbeda ini. Lapisan atas akan memberikan data dan kendali ke lapisan dibawahnya sampai lapisan yang terendah dicapai. Antara dua lapisan yang berdekatan terdapat interface (antarmuka). Interface ini mendifinisikan operasi dan layanan yang diberikan olehnya ke lapisan lebih atas. Tiap lapisan harus melaksanakan sekumpulan fungsi khusus yang dipahami dengan sempurna. Himpunan lapisan dan protokol dikenal sebagai "arsitektur jaringan".

4).SQL dan NoSQL
SQL menggunakan structured query language (SQL) untuk mendefinisikan dan memanipulasi data. Dengan kata lain, hal ini sangan powerful. Bisa dikatan SQL adalah suatu pilihan yang aman dan baik untuk query yang kompleks. Untuk hal query SQL memang jauh unggul, karena SQL sangat mudah untuk mengubah dari satu query ke query yang lainnya. SQL mengharuskan kita mendefiniskan schemas untuk menetukan struktur dari data sebelum kita bisa menggunakannya. Semua data kita harus mengikuti struktur yang sama. Seperti Perumahan A, it can mean that a change in the structure would be both difficult and disruptive to your whole system.
NoSQL memiliki dynamic schema for unstructured data dan data bisa di stored dengan banyak cara , whether it be graph-based, document-oriented, column-oriented, atau organized as a KeyValue store. This flexibility means that:
Kita bisa membuat dokumen tampa harus mendefiniskan structure nya
Setiap dokumen memiliki strukturnya sendiri
Syntax nya bisa berbedea dari database ke database, dan
Kita bisa menambahkan fields as you go.
Skalabilitas
Sekarang bayangkan jika kita ingin memperlebar perumahan.Jika kita andaikan sebuah perumahan SQL database seperti menambah lantai dari suatu gedung, yang artinya. SQL database dapat di scalable secara vertikal, kita dapat meningkatkan load suatu server dengan menambah CPU, RAM atau SSD. NoSQL database seperti kita menambah gedung lain di perumahan kita dibanding menambah lantai di satu rumah, NoSQL datapat di scalablle secara horizontal, kita dapat meningkatkan traffic dengan memecah atau menambah server di NoSQL database. Hal ini menjadikan NoSQL merupakan pilihan yang tepat untuk data yang besar dan terus berubah
Struktur
Akhirnya, hal terakhir yang perlu dipertimbangkan ketika memperdebatkan SQL vs NoSQL adalah strukturnya. Database SQL berbasis tabel yang menjadikannya pilihan yang lebih baik untuk aplikasi yang membutuhkan transaksi multi-rows. Database NoSQL dapat berupa key-value pairs, wide-column stores, graph databases, atau document-based

Kesimpulannya, Untuk hal query SQL memang jauh unggul, karena SQL sangat mudah untuk mengubah dari satu query ke query yang lainnya. Namun NoSQL hal tersebut sangatlah rumit, NoSQL lebih cocok untuk transaksi yang sama. Dari tabel diatas jika dianalisa maka dapat dikatakan SQL sangatlah cocok untuk transaksi harian dan NoSQL sangatlah cocok jika diterapkan pada transaksi histori.

5).Minimal 5 perintah git
cara saya pribadi untuk upload ke gitlab menggunakan https karena lebih simpel daripada ssh , kuncinya adalah pemilihan mode ssh yaitu git remote set-url origin ,caranya adalah =  
git config --global user.name "Ibrahim Hasan"
git config --global user.email "neodimium60144@gmail.com"
git init
git add .
git commit -m "Initial commit"
 git remote set-url origin https://gitlab.com/neodimium60144/tugas-devops_ibrahim_hasan.git
git commit
git push -u origin master
Perintah dasar GIT
git config
 Salah satu perintah git yang paling banyak digunakan adalah git config, yang bisa digunakan untuk mengatur konfigurasi tertentu sesuai keinginan pengguna, seperti email, algoritma untuk diff, username, format file, dll. Contohnya, perintah berikut bisa digunakan untuk mengatur email:
git config --global user.email sam@google.com
git init
 Perintah ini digunakan untuk membuat repositori baru. Caranya:
git init
git add
 Perintah git add bisa digunakan untuk menambahkan file ke index. Contohnya, perintah berikut ii akan menambahkan file bernama temp.txt yang ada di direktori lokal ke index:
git add temp.txt
git clone
 Perintah git clone digunakan untuk checkout repositori. Jia repositori berada di remove server, gunakan:
git clone alex@93.188.160.58:/path/to/repository
Jika salinan repositori lokal ingin dibuat, gunakan:
git clone /path/to/repository
git commit
 Perintah git commit digunakan untuk melakukan commit pada perubahan ke head. Ingat bahwa perubahan apapun yang di-commit tidak akan langsung ke remote repository. Gunakan:
git commit –m “Isi dengan keterangan untuk commit”
git status
 Perintah git status menampilkan daftar file yang berubah bersama dengan file yang ingin di tambahkan atau di-commit. Gunakan:
git status
git push
 git push adalah perintah git dasar lainnya. Push akan mengirimkan perubahan ke master branch dari remote repository yang berhubungan dengan direktori kerja Anda. Misalnya:
git push origin master
git checkout
 Perintah git checkout bisa digunakan untuk membuat branch atau untuk berpindah diantaranya. Misalnya, perintah berikut ini akan membuat branch baru dan berpindah ke dalamnya:
command git checkout -b <nama-branch>
Untuk berpindah dari branch satu ke lainnya, gunakan:
git checkout <branch-name>
git remote
 Perintah git remote akan membuat user terhubung ke remote repository. Perintah berikut ini akan menampilkan repository yang sedang dikonfigurasi:
git rmote -v
Perintah ini membuat user bisa menghubungkan repository lokal ke remote server:
git remote add origin <93.188.160.58>
git branch
 Perintah git branch bisa digunakan untuk me-list, membuat atau menghapus branch. Untuk menampilkan semua branch yang ada di repository, gunakan:
git branch
Untuk menghapus branch:
git branch -d <branch-name>
git pull
 Untuk menggabungkan semua perubahan yang ada di remote repository ke direktori lokal, gunakan perintah pull:
git pull
git merge
 Perintah merge digunakan untuk menggabungkan sebuah branch ke branch aktif. Gunakan:
git merge <nama-branch>
git diff
 Perintah git diff digunakan untuk menampilkan conflicts. Untuk melihat conflicts dengan file dasar, gunakan:
git diff --base <nama-file>
Perintah berikut digunakan untuk menampilkan conflicts diantara branch yang akan di-merge:
git diff <source-branch> <target-branch>
Untuk menampilkan semau conflict yang ada, gunakan:
git diff
git tag
 Tagging digunakan untuk menandai commits tertentu. Contohnya:
git tag 1.1.0 <insert-commitID-here>
git log
 Dengan menjalankan peritah ini akan menampilkan daftar commits yang ada di branch beserta detail-nya. Contoh outputnya adalah:
commit 15f4b6c44b3c8344caasdac9e4be13246e21sadw
Author: Alex Hunter <alexh@gmail.com>
Date: Mon Oct 1 12:56:29 2016 -0600
git reset
 Untuk me-reset index dan bekerja dengan kondisi commit paling baru, gunakan perintah git reset:
git reset --hard HEAD
git rm
 Gunakan perintah ini untuk menghapus file dari index dan direktori kerja. Contohnya:
git rm filename.txt
git stash
 Mungkin inilah salah satu perintah dasar git yang jarang digunakan orang, yang bisa membantu menyimpan perubahan yang tidak langsung di-commit, namun hanya sementara. Contoh:
git stash
git show
 Untuk menampilkan informasi tentang object pada git, gunakan git show:
git show
git fetch
 Perintah ini digunakan untuk menampilkan semua object dari remote repository yang tidak berada di direktori kerja lokal. Contohnya:
git fetch origin
git ls-tree
 Untuk menampilkan susunan object berdasarkan nama dan mode setiap item, dan nilai blob SHA-1, gunakan perintah git ls-tree. Contohnya:
git ls-tree HEAD
git cat-file
 Menggunakan nilai SHA-1, menampilkan tipe object dengan menggunakan perintah git cat-file. Contohnya:
git cat-file –p d670460b4b4aece5915caf5c68d12f560a9fe3e4
git prep
 git prep mengizinkan pengguna mencari frase dan/atau kata yang berada di dalam direktori. Contohnya, untuk mencari www.hostinger.co.id di dalam semua file, gunakan:
git grep "www.hostinger.co.id"
gitk
 gitk adalah tampilan grafis dari repository lokal yang bisa dipanggil dengan menjalankan perintah:
gitk
git instaweb
 Dengan perintah git instaweb, web server bisa dijalan berdampingan dengan repository lokal. Nantinya web browser akan langsung diarahkan ke server tersebut. Contohnya:
git instaweb –httpd=webrick
git gc
 Untuk mengoptimasi repository melalui garbage collection, yang akan membersihkan file yang tidak dibutuhkan dan mengoptimasinya, gunakan:
git gc
git archive
 Perintah git archive memungkinkan user membuat file zip atau tar yang mengandung susunan repository. Contohnya:
git archive --format=tar master
git prune
 Melalui perintah git prune, object yang tidak memiliki incoming pointers akan dihapus. Gunakan:
git prune
git fsck
 Untuk membuat pengecekan keseluruhan dari file system git, gunakan perintah git fsck. Object yang corrupt akan dikenali:
git fsck
git rebase
 Perintah ini digunakan untuk menerapkan ulang commit di branch yang lain. Contohnya:
git rebase master



6). gitlab = 
github = 
bitbucket = 

kalo menurut saya perbedaan nya adalah gitlab dan bitbucket mempunyai fitur ci/cd bawaan dan github tidak ada maka harus ada third party seperti travis CI dll

7). Step by Step Heroku
git clone https://github.com/sgnd/todo.git
cd todo
sudo apt-get install software-properties-common # debian only
sudo add-apt-repository "deb https://cli-assets.heroku.com/branches/stable/apt ./"
curl -L https://cli-assets.heroku.com/apt/release.key | sudo apt-key add -
sudo apt-get update
sudo apt-get install heroku
npm init -y
npm install node-static --save
npm install
code .
npm start
 git init
git remote -v
 git remote add github https://github.com/sgnd/todo.git
git init
heroku create devops-ibrahim
git add .
git commit -m "push proyek ke heroku"
git push heroku master
git config --global user.name "Ibrahim Hasan"
git config --global user.email "neodimium60144@gmail.com"
git init
git add .
git commit -m "Initial commit"
 git remote set-url origin https://gitlab.com/neodimium60144/tugas-devops_ibrahim_hasan.git
git commit
git push -u origin master


























